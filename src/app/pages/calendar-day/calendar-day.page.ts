/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { CalendarDayEditionPage } from '../calendar-day-edition/calendar-day-edition.page';
import { SequencesService } from '../../services/sequences.service';
import { ISequence, IEvent } from '../../services/pixapi.service';

@Component({
  selector: 'app-calendar-day',
  templateUrl: './calendar-day.page.html',
  styleUrls: ['./calendar-day.page.scss'],
})
export class CalendarDayPage {
  dateStr: string;
  sequences: ISequence[];
  @Input() dayData: IEvent[];
  @Input() date: Date;
  nowDate: Date;
  mode: string;

  constructor( public navParams: NavParams, private modalCtrl: ModalController,
               private seqSvc: SequencesService) {
    this.date = navParams.get('date');
    // console.log('in calendar day page, raw date: ' + this.date);
    this.dateStr = new Date(this.date.getTime() - (this.date.getTimezoneOffset() * 60000)).toISOString();
    this.dayData = navParams.get('dayData');
    if (this.dayData === undefined) {
      this.dayData = [];
    }
    this.nowDate = new Date();
    if ((this.nowDate.getFullYear() > this.date.getFullYear()) ||
    (this.nowDate.getFullYear() === this.date.getFullYear() && this.nowDate.getMonth() > this.date.getMonth()) ||
    (this.nowDate.getFullYear() === this.date.getFullYear() && this.nowDate.getMonth() === this.date.getMonth() &&
    this.nowDate.getDate() > this.date.getDate())) {
      this.mode = 'view';
    } else {
      this.mode = 'edition';
    }
  }
  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }
  /*! create new rendez vous */
  async newRendezVous() {
    const modal = await this.modalCtrl.create({
      component: CalendarDayEditionPage,
      componentProps: {
        date: this.date, // passing date without time
        new: true, // passing JSON data if exists
        data: {
          id: 0,
          datetime: this.date,
          description: '',
          sequence_id: 0,
          sequences: [],
          user_id: ''
        }
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.modified !== undefined) { // we modified one event
      if(this.dayData === undefined) {
        this.dayData = [];
      }
      this.dayData.push(data.modified);
    }
    return;
  }
  /*! edit existing rendez vous */
  async editRendezVous(myDay) {
    const modal = await this.modalCtrl.create({
        component: CalendarDayEditionPage,
        componentProps: {
          date: this.date, // passing date without time
          new: false, // passing JSON data if exists
          data: myDay as IEvent
        },
        cssClass: 'sequences-modal-class'
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.removed !== undefined) { // we removed one event
      for (let i = 0; i < this.dayData.length; i++) {
        if (data.removed.id === this.dayData[i].id) {
          this.dayData.splice(i, 1);
        }
      }
    }
    if (data.modified !== undefined) { // we modified one event
      for (let i = 0; i < this.dayData.length; i++) {
        if (data.modified.id === this.dayData[i].id) {
          this.dayData[i] = data.modified;
        }
      }
    }
    return;
  }
}
