/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { CalendarComponentOptions, DayConfig, CalendarComponent } from 'ion2-calendar';
import * as moment from 'moment';
import { RendezvousService } from '../../services/rendezvous.service';
import { ModalController, ToastController } from '@ionic/angular';
import { CalendarDayPage } from '../calendar-day/calendar-day.page';
import { CalendarDayEditionPage } from '../calendar-day-edition/calendar-day-edition.page';
import { IEventsList, IEvent } from '../../services/pixapi.service';
import { IRdvzList } from '../../components/rendezvous-list/rendezvous-list.component';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.page.html',
  styleUrls: ['./calendar.page.scss'],
})

export class CalendarPage implements OnInit {
  // calendar options
  date: string; // selected date
  type: 'string'; // 'string' | 'js-date' | 'moment' | 'time' | 'object' (selected date type)
  daysLoaded: IEvent[][];
  daysList: IRdvzList[];
  calendarView: string;
  optionsMulti: CalendarComponentOptions = {
    pickMode: 'single',
    monthPickerFormat: [
      'Enero',
      'Febrero',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Agosto',
      'Septiembre',
      'Octubre',
      'Noviembre',
      'Diciembre'
    ],
    monthFormat: 'MMMM YYYY',
    weekdays: [
      'D',
      'L',
      'M',
      'X',
      'J',
      'V',
      'S'
    ],
    weekStart: 1,
    daysConfig: []
  };

  constructor( private rendezVous: RendezvousService, public modalCtrl: ModalController,
               private localNotifications: LocalNotifications) {
    console.log ('dentro de la página');
    moment.locale('es-es');
    // initial view as calendar
    this.calendarView = 'calendar';
    this.localNotifications.on('click').subscribe(
    (notification) => {
      console.log ('localNotifications starts');
      const json = JSON.parse(notification.data);
      this.callDayEvent(json);
      console.log ('localNotifications finishes');
    });
    console.log ('end constructor');
  }
/*! opening rendezvous page when local notification trigger*/
  async callDayEvent(json: any) {
    const modal = await this.modalCtrl.create({
      component: CalendarDayEditionPage,
      componentProps: {
        date: json.data.date, // passing date without time
        new: false, // passing JSON data if exists
        data: json.data.event
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
  }

  ngOnInit() {
    this.setCalendarView(this.calendarView);
    this.updateCalendar();
  }

  onChange($event) {
    console.log($event);
    this.rendezVous.getEvents().then(
      (val) => {
        console.log(val);
      },
      (err) => {
        console.log(err);
    });
  }
  /*! day selection event*/
  async onSelect($event) {
    const dayDate = new Date($event['time']);
    const modal = await this.modalCtrl.create({
      component: CalendarDayPage,
      componentProps: {
        date: dayDate, // passing date without time
        dayData: this.daysLoaded[dayDate.getTime()] // passing JSON data if exists
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    console.log(data);
    if (data.dismissed === true) {
      console.log('updating!');
      this.updateCalendar();
    }
    return;
  }
  /*! function for upgrading calendar options */
  updateCalendar() {
    this.rendezVous.getEvents().then(
      (val) => {
        const daysConfig: DayConfig[] = [];
        this.daysLoaded = [];
        this.daysList = [];
        // dayConfig for ion2-calendar
        for (const dayData of val.data) {
          daysConfig.push({
            cssClass: 'eventDay',
            date: dayData.datetime,
            marked: true,
            // title: 'cita'
          });

          // we need an absolute index for storing our events
          // console.log('dayData datetime: ' + dayData.datetime);
          const dateIndex = new Date(dayData.datetime);
          // console.log('dayData datetime: ' + dateIndex);
          const dateIndexNum = new Date(dateIndex.getFullYear(), dateIndex.getMonth(), dateIndex.getDate()).getTime();
          // storing our rendez vous matrix
          if (this.daysLoaded[dateIndexNum as any as number] === undefined) {
            this.daysLoaded[dateIndexNum as any as number] = [];
          }
          this.daysLoaded[dateIndexNum as any as number].push({
            datetime: dayData.datetime,
            description: dayData.description,
            id: dayData.id,
            sequence_id: dayData.sequence_id,
            sequences: [],
            user_id: dayData.user_id
          });
          // local notifications
          if ( new Date() < new Date(dayData.datetime)) {
            this.localNotifications.schedule({
              id: dayData.id,
              title: 'Tienes una cita en una hora',
              text: dayData.description,
              data: { date: dayData.datetime, event: this.daysLoaded[dateIndexNum as any as number]},
              trigger: {at: new Date(new Date(dayData.datetime).getTime() - 60 * 1000)}
            });
            this.localNotifications.schedule({
              id: -dayData.id,
              title: 'Tienes una cita mañana',
              text: dayData.description,
              data: { date: dayData.datetime, event: this.daysLoaded[dateIndexNum as any as number]},
              trigger: {at: new Date(new Date(dayData.datetime).getTime() - 3600 * 1000)}
            });
          }
          //
          // rendezvous list
          this.daysList.push({
            day: (new Date(dayData.datetime)).getDate(),
            month: this.optionsMulti.monthPickerFormat[(new Date(dayData.datetime)).getMonth()],
            year: (new Date(dayData.datetime)).getFullYear(),
            dayData: {
              datetime: dayData.datetime,
              description: dayData.description,
              id: dayData.id,
              sequence_id: dayData.sequence_id,
              sequences: [],
              user_id: dayData.user_id
            }
          });
          // arrange same date rendezvous by hour
          const tmpDaysLoaded: IEvent[] = this.daysLoaded[dateIndexNum as any as number].sort((n1, n2) => {
            if (n1.datetime > n2.datetime) {
              return 1;
            }
            if (n1.datetime < n2.datetime) {
              return -1;
            }
            return 0;
          });
          this.daysLoaded[dateIndexNum as any as number] = tmpDaysLoaded;
        }
        setTimeout(() => {
          /*
          const newOptionsMulti = {
            pickMode: 'single',
            monthPickerFormat: [
              'Enero',
              'Febrero',
              'Marzo',
              'Abril',
              'Mayo',
              'Junio',
              'Julio',
              'Agosto',
              'Septiembre',
              'Octubre',
              'Noviembre',
              'Diciembre'
            ],
            monthFormat: 'MMMM YYYY',
            weekdays: [
              'D',
              'L',
              'M',
              'X',
              'J',
              'V',
              'S'
            ],
            weekStart: 1,
            daysConfig: [] = daysConfig
          };

          // follows this
          this.optionsMulti = {
              ...this.optionsMulti,
              ...newOptionsMulti
          };
          */
          this.optionsMulti = {
            ...this.optionsMulti,
            daysConfig: [] = daysConfig
          };
        }, 2000);
      },
      (err) => {
        console.log(err);
    });
  }
  /*! function for radio buttons, for upgrading CSS*/
  setCalendarView(option) {
    if (option === 'calendar') {
      this.calendarView = 'calendar';
      let el = document.querySelector('#calendar-button') as HTMLElement;
      el.style.setProperty('color', 'var(--ion-color-primary)');
      el = document.querySelector('#list-button') as HTMLElement;
      el.style.setProperty('color', 'var(--ion-text-color)');
    } else {
      this.calendarView = 'list';
      let el = document.querySelector('#calendar-button') as HTMLElement;
      el.style.setProperty('color', 'var(--ion-text-color)');
      el = document.querySelector('#list-button') as HTMLElement;
      el.style.setProperty('color', 'var(--ion-color-primary)');
    }
  }
}
