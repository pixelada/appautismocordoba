/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Component, OnInit, Input } from '@angular/core';
import { ISequence, PixapiService, IPictogram } from '../../services/pixapi.service';
import { ModalController, NavParams, AlertController } from '@ionic/angular';
import { SequencesService } from '../../services/sequences.service';
import { PictogramsService } from '../../services/pictograms.service';

@Component({
  selector: 'app-sequence-edition',
  templateUrl: './sequence-edition.page.html',
  styleUrls: ['./sequence-edition.page.scss'],
})
export class SequenceEditionPage implements OnInit {
@Input() new: boolean;
@Input() seqData: ISequence;
step: number;
allPictograms: IPictogram[];
editable: boolean;

  constructor(private modalCtrl: ModalController, private navParams: NavParams,
              private atrCtrl: AlertController, private pixApi: PixapiService,
              private seqSvc: SequencesService, private pictoSvc: PictogramsService) {
    this.new = this.navParams.get('new');
    this.seqData = this.navParams.get('seqData');
    this.step = 0;
    this.editable = true;
    pictoSvc.getPictograms().then(
      (val) => {
        this.allPictograms = val as IPictogram[];
      },
      (err) => {
        this.allPictograms = err;
    });
    if (this.new === true) {
      this.seqData.id = 0;
      this.seqData.pictograms = [];
    } else {
      this.editable = this.navParams.get('editable');
      this.step = 2;
      this.loadSeq();
    }
  }
  ngOnInit() {
  }
  /*! load sequence data */
  loadSeq() {
    this.seqSvc.getOneSequence(this.seqData.id).then(
    (val) => {
      this.seqData = (val as any) as ISequence;
      this.seqData.pictograms = (val as any).pictograms as IPictogram[];
    });
  }
  /*! sequence update function */
  onUpdatedPicto($event) {}

  /*! next step function when creating a new sequence */
  next() {
    let tmpTwoSteps = true;
    let errMsg = '';

    if (this.seqData.title === '') {
      tmpTwoSteps = false;
      errMsg = 'No has rellenado el título de la secuencia';
    }
    if (this.seqData.description === '') {
      if (!tmpTwoSteps) {
        errMsg = errMsg + ' y tampoco';
      } else {
        errMsg = errMsg + 'No';
      }
      tmpTwoSteps = false;
      errMsg = errMsg + ' has rellenado la descripción de la secuencia';
    }
    if (!tmpTwoSteps) {
      this.showAlert('rellena bien los campos primero', errMsg);
    } else {
      this.step = 1;
      this.seqSvc.newSequence(this.seqData).then(
        (id) => {
          this.seqData.id = id as number;
        },
        (err) => {
          this.seqData.id = 0; this.step = 0;
      });
    }
  }
  /*! function for stepping back in form*/
  back() {
    this.seqSvc.delSequence(this.seqData.id).then(
      (deletionOk) => {
          this.step = 0;
      },
      (deletionError) => {
        console.log('what we do? we can\'t delete!');
    });
  }
  cancel() {
    this.step = 2;
    // back to previous data
    this.loadSeq();
  }
  /*! function for editing a sequence*/
  edit() {
    this.step = 3;
  }
  /*! function for saving sequence data from form*/
  save() {
    if (this.new) {
      this.saveConf();
    } else {
      this.showConfirm('save', 'Vas a modificar la secuencia de pasos, esta acción no tiene vuelta atrás, confirma:');
    }
  }
  /*! function for saving sequence data from form after user confirmation */
  saveConf() {
    this.seqSvc.editSequence(this.seqData).then (
      (ok) => {
        this.modalCtrl.dismiss({
          dismissed: true,
          modified: this.seqData,
          removed: undefined
        });
      },
      (err) => {
         // TODO: error message?
    });
  }
  /*! function for erasing sequence data*/
  delete() {
    this.showConfirm('delete', 'Vas a borrar la secuencia, esta acción no tiene vuelta atrás, confirma:');
  }
  /*! function for erasing sequence data, when confirmed*/
  deleteConf() {
  this.seqSvc.delSequence(this.seqData.id).then (
    (ok) => {
      this.modalCtrl.dismiss({
        dismissed: true,
        removed: this.seqData,
        modified: undefined
      });
    },
      (err) => {
        this.showAlert('Error', 'Esta secuencia no se pudo borrar, vuelve a intentarlo, error:' + err);
    });
  }
  /*! function for dismissing and don't do anything */
  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true,
      removed: undefined,
      modified: undefined
    });
  }
  /*! function for showing popup alerts*/
  async showAlert(title: string, message: string) {
    const alert = await this.atrCtrl.create({
      header: title,
      message,
      buttons: ['OK']
    });
    await alert.present();
  }

  /*! function for showing popup confirming dialogs*/
  async showConfirm( type: string, message: string) {
    const alert = await this.atrCtrl.create({
      header: 'Confirma los cambios',
      message,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            // void?
          }
        }, {
          text: 'Ok',
          handler: () => {
            if (type === 'delete') {
              this.deleteConf();
            } else {
              this.saveConf();
            }
          }
        }
      ]
    });

    await alert.present();
  }
}
