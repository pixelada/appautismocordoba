/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { TipsService } from '../../services/tips.service';
import { PixapiService } from '../../services/pixapi.service';

@Component({
  selector: 'app-tips',
  templateUrl: './tips.page.html',
  styleUrls: ['./tips.page.scss'],
})
export class TipsPage implements OnInit {
  title = 'Consejos';
  toggles: any[];
  toggle2: any;
  tips: any;
  lastId: any;
  IMAGES_URL: string;
  constructor(public myTips: TipsService, public myApi: PixapiService) {
    this.toggles = [];
    this.lastId = -1;
    this.IMAGES_URL = this.myApi.BASE_URL;
  }

  ngOnInit() {
    this.myTips.getTips().then(
      (tipsList) => {
        this.tips = tipsList;
      },
      (error) => {
        console.log('error!:', error);
    });
  }
  toggle(id) {
    if (this.toggles[id]) {
      this.toggles[id] = !this.toggles[id];
    } else {
      this.toggles[id] = true;
    }
    if ( this.lastId >= 0 && this.lastId !== id ) {
      this.toggles[this.lastId] = false;
    }
    this.lastId = id;
  }

}
