/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Component, OnInit } from '@angular/core';
import { IPictogram } from '../../services/pixapi.service';
import { ModalController, NavParams } from '@ionic/angular';
import { PictogramsService } from '../../services/pictograms.service';

@Component({
  selector: 'app-choose-picto',
  templateUrl: './choose-picto.page.html',
  styleUrls: ['./choose-picto.page.scss'],
})
export class ChoosePictoPage implements OnInit {
  allPictos: IPictogram [];
  selectedPicto: IPictogram = {
    id: 0,
    image: '',
    description: ''
  };
  constructor(private modalCtrl: ModalController, private navParams: NavParams,
              private pictosSvc: PictogramsService) {
    this.allPictos = navParams.get('allPictograms');
    if (navParams.get('mode') === 'edit' && navParams.get('selectedPictogram') !== undefined) {
      this.selectedPicto = navParams.get('selectedPictogram');
    }
    this.selectedPicto.id = 0;
    for (let i = 0; i < this.allPictos.length; i++) {
      this.pictosSvc.getOnePictogram(this.allPictos[i].id).then(
      (pictoData) => {
        this.allPictos[i] = pictoData;
        if (pictoData.id === this.selectedPicto.id) {
          this.selectedPicto.id = i;
        }
    });
  }
}

ngOnInit() {
}
/*! function for catching the event emitter from sequences component */
onSelectedPicto(newPicto: IPictogram) {
  this.selectedPicto = newPicto;
}
/*! function for closing modal dialog returning selected id */
choose() {
  this.modalCtrl.dismiss({
    dismissed: true,
    pictogram: this.selectedPicto
  });
}
/*! function for closing modal dialog */
cancel() {
  this.modalCtrl.dismiss({
    dismissed: true,
    pictogram: undefined
  });
}

}
