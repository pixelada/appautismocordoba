/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session.service';
import { AlertController, NavController, MenuController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  name: string;
  email: string;
  pass: string;
  passwordType = 'password';
  passwordIcon = 'eye-off';

  constructor( public session: SessionService, public atrCtrl: AlertController,
               public navCtrl: NavController, public menuCtrl: MenuController) { }

  ngOnInit() {
    this.menuCtrl.enable(false);
  }

  register() {
    // do we validate data
    // does the api validate the data?
    // once validated register
    // console.log('DEBUG: sending name:' + this.name + ' email:' + this.email + ' password:' + this.pass);
    this.session.register(this.name, this.email, this.pass)
    .then(
      (val) => {
        console.log('done!' + val);
        // sending INFO message
        this.showAlert('¡Atención!', 'se ha mandado un correo electrónico de confirmación a la cuenta de email suministrada.');
        this.navCtrl.navigateBack('/login');
      },
      (err) => {
        console.log('error!' + err);
        this.showAlert('¡error!', err);
      }
    );
  }

  /*! function for showing or hiding password insertion */
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  /*! function for showing popup alerts*/
  async showAlert(title: string, message: string) {
    const alert = await this.atrCtrl.create({
      header: title,
      message,
      buttons: ['OK']
    });
    await alert.present();
  }


}
