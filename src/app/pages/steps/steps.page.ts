/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { ISequence } from '../../services/pixapi.service';
import { SequencesService } from '../../services/sequences.service';
import { AlertController, ModalController } from '@ionic/angular';
import { SequenceEditionPage } from '../../pages/sequence-edition/sequence-edition.page';

@Component({
  selector: 'app-steps',
  templateUrl: './steps.page.html',
  styleUrls: ['./steps.page.scss'],
})
export class StepsPage implements OnInit {
  allSequences: ISequence[];

  constructor(private seqSrv: SequencesService, private atrCtrl: AlertController,
              private seqSvc: SequencesService, private modalCtrl: ModalController) {
    this.loadSeqs();
  }

  ngOnInit() {
  }
  /*! updating function*/
  loadSeqs() {
    this.seqSrv.getSequences().then(
      (val) => {
        this.allSequences = val as ISequence[];
      },
      (err) => {
        // TODO: nothing cached
        this.allSequences = [];
        console.log(err);
    });
  }
  /* function for receiving an adding new sequence event */
  onNewSeq(seqs: ISequence[]) {
    this.loadSeqs();
  }
  /* function for receiving a removing sequence event */
  onRemoveSeq(seqToRemove: ISequence) {
    this.showConfirm('delete', 'Vas a borrar la secuencia, esta acción no tiene vuelta atrás, confirma:', seqToRemove);
  }
  /*! function for erasing sequence data, when confirmed*/
  deleteConf(mySeq: ISequence) {
    this.seqSvc.delSequence(mySeq.id).then (
      (ok) => {
        this.showAlert('Hecho', '¡Secuencia borrada!');
        this.loadSeqs();
      },
      (err) => {
        this.showAlert('Error', 'Esta secuencia no se pudo borrar, vuelve a intentarlo, error:' + err);
      });
  }
  /* function for adding a new sequence */
  async onEditSeq(seqToEdit: ISequence) {
    const modal = await this.modalCtrl.create({
      component: SequenceEditionPage,
      componentProps: {
        new: false, // new item
        seqData: seqToEdit,
        editable: true
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.modified !== undefined) { // we modified one sequence
      for (let i = 0; i < this.allSequences.length; i++) {
        if (data.modified.id === this.allSequences[i].id) {
          this.allSequences[i] = data.modified;
        }
      }
    }
    if (data.removed !== undefined) {
      for (let i = 0; i < this.allSequences.length; i++) {
        if (data.modified.id === this.allSequences[i].id) {
          this.allSequences.splice(i, 1);
        }
      }
    }
    return;
  }
  /* function for adding a new sequence */
  async onViewSeq(seqToView: ISequence) {
    const modal = await this.modalCtrl.create({
      component: SequenceEditionPage,
      componentProps: {
        new: false, // new item
        seqData: seqToView,
        editable: false
      },
      cssClass: 'sequences-modal-class'
    });
    await modal.present();
    return;
  }
  /*! function for showing popup alerts*/
  async showAlert(title: string, message: string) {
    const alert = await this.atrCtrl.create({
      header: title,
      message,
      buttons: ['OK']
    });
    await alert.present();
  }
  /*! function for showing popup confirming dialogs*/
  async showConfirm( type: string, message: string, mySeq: ISequence) {
    const alert = await this.atrCtrl.create({
      header: 'Confirma los cambios',
      message: message,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            // void?
          }
        },
        {
          text: 'Ok',
          handler: () => {
            if (type === 'delete') {
              this.deleteConf(mySeq);
            }
          }
        }
      ]
    });
    await alert.present();
  }
}
