/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ModalController, NavParams, AlertController } from '@ionic/angular';
import { RendezvousService } from '../../services/rendezvous.service';
import { ISequence, IEvent, PixapiService } from '../../services/pixapi.service';
import { SequencesService } from '../../services/sequences.service';

@Component({
  selector: 'app-calendar-day-edition',
  templateUrl: './calendar-day-edition.page.html',
  styleUrls: ['./calendar-day-edition.page.scss'],
})

export class CalendarDayEditionPage implements OnInit {
  @Input() date: Date;
  @Input() new: boolean;
  @Input() dayData: IEvent;
  eventDate: string;
  step: number;
  allSequences: ISequence[] = [];
  rdvzSequences: ISequence[] = [];

  constructor(private modalCtrl: ModalController, private navParams: NavParams,
              private atrCtrl: AlertController, private rendezvousSvc: RendezvousService,
              private pixApi: PixapiService, private seqSvc: SequencesService,
              private changeRef: ChangeDetectorRef) {
    this.date = this.navParams.get('date');
    this.new = this.navParams.get('new');
    this.dayData = this.navParams.get('data');
    this.step = 0;
    seqSvc.getSequences().then(
      (val) => {
        this.allSequences = val as ISequence[];
      },
      (err) => {
        this.allSequences = err;
    });
    if (this.new === true) {
      // console.log('is new event');
      this.eventDate = new Date(this.date.getTime() - (this.date.getTimezoneOffset() * 60000)).toISOString();
      this.dayData.datetime = this.date;
      this.dayData.id = 0;
      this.dayData.sequences = [];
      // console.log(this.eventDate);
    } else {
      // console.log('is NOT new event');
      this.eventDate = new Date(this.dayData.datetime).toISOString();
      if (new Date(this.dayData.datetime) > new Date()) { // day not passed
        this.step = 2;
      } else { // day passed!
        this.step = 4;
      }
      this.loadDay();
    }
  }
  ngOnInit() {

  }
  /*! next step function when creating a new rendezvous */
  next() {
    let tmpTwoSteps = true;
    let errMsg = '';

    if (!this.titleIsOk()) {
      tmpTwoSteps = false;
      errMsg = 'No has rellenado el título de la cita';
    }
    if (!this.dateIsOk()) {
      if (!tmpTwoSteps) {
        errMsg = errMsg + ' y tampoco';
      } else {
        errMsg = errMsg + 'No';
      }
      tmpTwoSteps = false;
      errMsg = errMsg + ' es correcta la hora, ¿habrá pasado ya?';
    }
    if (!tmpTwoSteps) {
      this.showAlert('rellena bien los campos primero', errMsg);
    } else {
      this.step = 1;
      this.dayData.datetime = new Date(this.eventDate);
      this.rendezvousSvc.newEvent(this.eventDate, this.dayData.description).then(
        (id) => {
          this.dayData.id = id as number;
        },
        (err) => {
          this.dayData.id = 0; this.step = 0;
      });
    }
  }
  /*! load day data */
  loadDay() {
    this.rendezvousSvc.getOneEvent(this.dayData.id).then(
    (val) => {
      this.dayData = (val as any).data as IEvent;
      this.rdvzSequences = (val as any).data.sequences as ISequence[];
    });
  }
  /*! function for checking if the event title is inserted */
  titleIsOk(): boolean {
    if (!this.dayData.description) {
      return false;
    } else {
      return true;
    }
  }
   /*! function for checking if the event date is possible */
   dateIsOk(): boolean {
    const eventDateDate = new Date(this.eventDate);
    // updating if today date
    const nowDate = new Date();
    /* if (this.date < nowDate) {
      this.date = nowDate;
    } */
    if (eventDateDate > nowDate) { // everything ok
      return true;
    } else { // old date
      return false;
    }
  }
  /* function for adding a new sequence */
  onUpdatedSeq(newSeqs: ISequence[]) {
    if (newSeqs !== undefined) {
      this.rdvzSequences = newSeqs;
      this.dayData.sequences = newSeqs;
    }
  }
  /*! function for stepping back in form*/
  back() {
    this.rendezvousSvc.delEvent(this.dayData.id).then(
      (deletionOk) => {
        this.step = 0;
      },
      (deletionError) => {
        console.log('what we do? we can\'t delete!');
      });
  }
  /*! function for saving event data from form*/
  save() {
    if (this.new) {
      this.saveConf();
    } else {
      this.showConfirm('save', 'Vas a modificar el evento, esta acción no tiene vuelta atrás, confirma:');
    }
  }
  /*! function for saving event data from form after user confirmation */
  saveConf() {
    this.rendezvousSvc.editEvent(this.dayData).then (
      (ok) => {
        this.modalCtrl.dismiss({
          dismissed: true,
          modified: this.dayData,
          removed: undefined
        });
      },
      (err) => {
        // TODO: error message?
    });
  }
  /*! function for erasing event data*/
  delete() {
    this.showConfirm('delete', 'Vas a borrar el evento, esta acción no tiene vuelta atrás, confirma:');
  }
  /*! function for erasing event data, when confirmed*/
  deleteConf() {
  this.rendezvousSvc.delEvent(this.dayData.id).then (
    (ok) => {
      this.modalCtrl.dismiss({
        dismissed: true,
        removed: this.dayData,
        modified: undefined
      });
    },
      (err) => {
        this.showAlert('Error', 'Este evento no se pudo borrar, vuelve a intentarlo, error:' + err);
    });
  }
  cancel() {
    this.step = 2;
    // back to previous data
    this.loadDay();
  }
  /*! function for dismissing and don't do anything */
  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true,
      removed: undefined,
      modified: undefined
    });
  }
  /*! function for editing a rendezvous*/
  edit() {
    this.step = 3;
  }
  /*! function for showing popup alerts*/
   async showAlert(title: string, message: string) {
    const alert = await this.atrCtrl.create({
      header: title,
      message,
      buttons: ['OK']
    });
    await alert.present();
  }

  /*! function for showing popup confirming dialogs*/
  async showConfirm( type: string, message: string) {
    const alert = await this.atrCtrl.create({
      header: 'Confirma los cambios',
      message,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            // void?
          }
        }, {
          text: 'Ok',
          handler: () => {
            if (type === 'delete') {
              this.deleteConf();
            } else {
              this.saveConf();
            }
          }
        }
      ]
    });

    await alert.present();
  }
}
