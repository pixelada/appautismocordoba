/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ISequence } from '../../services/pixapi.service';
import { SequencesService } from '../../services/sequences.service';

@Component({
  selector: 'app-choose-sequence',
  templateUrl: './choose-sequence.page.html',
  styleUrls: ['./choose-sequence.page.scss'],
})
export class ChooseSequencePage implements OnInit {
  allSequences: ISequence [];
  selectedSeq: ISequence = {
    author_id: '',
    description: '',
    id: 0,
    pictograms: [],
    title: ''
  };
  constructor(private modalCtrl: ModalController, private navParams: NavParams,
              private sequencesSvc: SequencesService) {
    this.allSequences = navParams.get('allSequences');
    if (navParams.get('mode') === 'edit' && navParams.get('selectedSequence') !== undefined) {
      this.selectedSeq = navParams.get('selectedSequence');
    }
    this.selectedSeq.id = 0;
    for (let i = 0; i < this.allSequences.length; i++) {
      sequencesSvc.getOneSequence(this.allSequences[i].id).then(
        (sequenceData) => {
          this.allSequences[i] = sequenceData;
          if (sequenceData.id === this.selectedSeq.id) {
            this.selectedSeq.id = i;
          }
      });
    }
  }

  ngOnInit() {
  }
  /*! function for catching the event emitter from sequences component */
  onSelectedSeq(newSequence: ISequence) {
    this.selectedSeq = newSequence;
  }
  /*! function for closing modal dialog returning selected id */
  choose() {
    this.modalCtrl.dismiss({
      dismissed: true,
      sequence: this.selectedSeq
    });
  }
  /*! function for closing modal dialog */
  cancel() {
    this.modalCtrl.dismiss({
      dismissed: true,
      sequence: undefined
    });
  }
}
