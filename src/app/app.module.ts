/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, NavParams } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoadingService } from './services/loading.service';
import { SessionService } from './services/session.service';
import { AutismoNewsService } from './services/autismo-news.service';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { ComponentsModule } from './components/components.module';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import {CacheModule} from 'ionic-cache-observable';
import { CalendarModule } from 'ion2-calendar';
import { FormsModule } from '@angular/forms';
import { CalendarDayEditionPage } from './pages/calendar-day-edition/calendar-day-edition.page';
import { ChooseSequencePage } from './pages/choose-sequence/choose-sequence.page';
import { CalendarDayPage } from './pages/calendar-day/calendar-day.page';
import { SequenceEditionPage } from './pages/sequence-edition/sequence-edition.page';
import { ChoosePictoPage } from './pages/choose-picto/choose-picto.page';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@NgModule({
  declarations: [
    AppComponent,
    CalendarDayPage,
    CalendarDayEditionPage,
    ChooseSequencePage,
    SequenceEditionPage,
    ChoosePictoPage
  ],
  entryComponents: [
    CalendarDayPage,
    CalendarDayEditionPage,
    ChooseSequencePage,
    SequenceEditionPage,
    ChoosePictoPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    CacheModule,
    FormsModule,
    ComponentsModule,
    CalendarModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LoadingService,
    SessionService,
    AutismoNewsService,
    InAppBrowser,
    LocalNotifications,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
