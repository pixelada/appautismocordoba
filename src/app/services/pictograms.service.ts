/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Injectable } from '@angular/core';
import { PixapiService, IPictogram } from './pixapi.service';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class PictogramsService {

  constructor(private myApi: PixapiService, private myStorage: Storage) { }
  /*function for getting a pictograms list*/
  getPictograms(): Promise<IPictogram[]> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.pictogramList(id).then(
            (jsonData) => {
              resolve(jsonData);
            },
            (jsonData) => {
              reject(jsonData);
          });
        },
        (idErr) => {
          reject(idErr);
      });
    });
  }
  /*function for getting one pictogram item data*/
  getOnePictogram(pictogramID): Promise<IPictogram> {
      return new Promise((resolve, reject) => {
        this.myStorage.get(this.myApi.SESSION_ID).then(
          (userId) => {
            this.myApi.pictogramItem(pictogramID, userId).then(
              (pictogram) => {
                resolve((pictogram as any).data);
              },
              (pictogramErr) => {
                reject(pictogramErr);
            });
          },
          (idErr) => {
            reject(idErr);
        });
      });
    }
}
