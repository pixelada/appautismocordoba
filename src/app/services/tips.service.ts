/* 
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *  
 *  This file is part of Dentaltea
 *  
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of AppAutismoCórdoba
 *
 *  AppAutismoCórdoba is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AppAutismoCórdoba is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AppAutismoCórdoba.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { PixapiService } from './pixapi.service';
import { Storage } from '@ionic/storage';



@Injectable({
  providedIn: 'root'
})
export class TipsService {

  constructor(public myApi: PixapiService, public myStorage: Storage) { }

  /*! function for getting a list of tips */
  getTips() {
    const myPromise = new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.tipsList(id).then(
            (tipsList) => { // returned tips list
              resolve(tipsList);
            },
            (listErr) => {
              reject(listErr);
          });
        },
        (idErr) => {
          reject(idErr);
      });
    });
    return myPromise;
  }
}
