import { Injectable } from '@angular/core';
import { PixapiService, ISequence } from './pixapi.service';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class SequencesService {

  constructor(private myApi: PixapiService, private myStorage: Storage) { }
  /*function for getting an events list*/
  getSequences(): Promise<ISequence[]> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.sequencesList(id).then(
            (sequenceList) => {
              resolve(sequenceList);
            },
            (listErr) => {
              reject(listErr);
          });
        },
        (idErr) => {
          /*
          const errorEventsList: IEventsList = {
            data: null,
            msg: 'bad user id',
            result: 'err'
          };
          */
          reject(idErr);
      });
    });
  }
  /*function for getting one event data*/
  getOneSequence(sequenceID): Promise<ISequence> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (userId) => {
          this.myApi.sequenceGet(sequenceID, userId).then(
            (sequence) => {
              resolve((sequence as any).data);
            },
            (sequenceErr) => {
              reject(null);
          });
        },
        (idErr) => {
          /*
          const errorEventsList: IEventsList = {
            data: null,
            msg: 'bad user id',
            result: 'err'
          };
          */
          reject(idErr);
      });
    });
  }
  /*function for creating a sequence*/
  newSequence(seq: ISequence): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.sequenceAdd(seq, id).then(
            (seqID) => {
              resolve(seqID as number);
            },
            (error) => {
              reject(0);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  /*function for editing a sequence*/
  editSequence(seq: ISequence): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.sequenceEdit(seq, id).then(
            (seqID) => {
              resolve(seqID as number);
            },
            (error) => {
              reject(0);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  /*function for erasing a sequence*/
  delSequence(seqId: number) {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
       (id) => {
          this.myApi.sequenceDelete(seqId, id).then(
            () => {
              resolve(true);
            },
            () => {
              reject(false);
            });
        },
        () => {
          const errorId = -1;
          reject(errorId);
        });
      });
    }
}
