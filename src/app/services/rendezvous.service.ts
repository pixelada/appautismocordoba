/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { PixapiService, IEventsList, IEvent } from './pixapi.service';

@Injectable({
  providedIn: 'root'
})

/*! class for managing calendar rendez vous */
export class RendezvousService {
  constructor(private myApi: PixapiService, private myStorage: Storage) { }

  /*function for getting an events list*/
  getEvents(): Promise<IEventsList> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.eventsList(id).then(
            (eventsList) => {
              resolve(eventsList);
            },
            (listErr) => {
              reject(listErr);
          });
        },
        (idErr) => {
          const errorEventsList: IEventsList = {
            data: null,
            msg: 'bad user id',
            result: 'err'
          };
          reject(errorEventsList);
      });
    });
  }
  /*function for getting an event item*/
  getOneEvent(id): Promise<IEvent> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (userId) => {
          this.myApi.eventGet(id, userId).then(
            (event) => {
              resolve(event);
            },
            (err) => {
              reject(err);
          });
        },
        (err) => {
          reject(null);
      });
    });
  }
  /*function for new event*/
  newEvent(date, description: string): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          const mysqlDate = this.myApi.dateForMysql(new Date(date)) +
                            ' ' + new Date(date).toTimeString().split(' ')[0];
          this.myApi.eventAdd(mysqlDate, description, id).then(
            (eventID) => {
              console.log(eventID);
              resolve(eventID as number);
            },
            (error) => {
              reject(0);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }

  /*function for editing an event, normally for editing it's sequences*/
  editEvent(event: IEvent): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.eventEdit(event, id).then(
            (eventID) => {
              console.log(eventID);
              resolve(eventID as number);
            },
            (error) => {
              reject(0);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  /*function for erasing an event*/
  delEvent(eventId) {
  return new Promise((resolve, reject) => {
    this.myStorage.get(this.myApi.SESSION_ID).then(
     (id) => {
        this.myApi.eventDelete(eventId, id).then(
          () => {
            resolve(true);
          },
          () => {
            reject(false);
          });
      },
      () => {
        const errorId = -1;
        reject(errorId);
      });
    });
  }
}
