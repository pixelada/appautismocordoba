/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { identifierModuleUrl } from '@angular/compiler';

declare var require: any;
// const TOKEN_KEY = '1235465a1c0058719712b867b67fa1e9'; // 'auth-token';
const TOKEN = require('../../../secrets.json');
const BASE_URL = 'https://dentaltea.autismocordoba.org/';
const API_URL = BASE_URL + 'api/';

export interface IEventsList {
  data: {
    datetime: Date;
    description: string;
    id: number;
    sequence_id: number;
    sequences: any[];
    user_id: string;
  }[];
  msg: string;
  result: string;
}

export interface IEvent {
  datetime: Date;
  description: string;
  id: number;
  sequence_id: number;
  sequences: ISequence[];
  user_id: string;
}

export interface ISequence {
  author_id: string;
  description: string;
  id: number;
  pictograms: IPictogram[];
  title: string;
}

export interface IPictogram {
  id: number;
  image: string;
  description: string;
}

@Injectable({
  providedIn: 'root'
})
export class PixapiService {
  // JSON AND STORE tags
  USER_NAME = 'name';
  USER_EMAIL = 'email';
  USER_CREATION_DATE = 'creation_date';
  USER_MD5_PASSWORD = 'pass';
  USER_PASSWORD = 'pass';
  USER_ID = 'id';
  SESSION_ID = 'session_id';
  TOKEN = 'token';
  //
  myData: any = [];
  BASE_URL = BASE_URL;

  constructor(public http: HttpClient) {}
  /*! login function, it takes email, pass and token, and try to do
      a login */
  login(email, password) {
    const myPromise = new Promise((resolve, reject) => {
      const url = API_URL + 'user/login';
      const jsonData = JSON.stringify({
        email,
        pass: password,
        token: TOKEN.KEY
      });
      console.log('DEBUG api login, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
        data => {
          console.log('DEBUG api login, receiving:', data);
          this.myData = data;
          console.log('DEBUG api login, transcription:', this.myData);
          if (this.myData.result === 'error') {
            reject(this.myData.msg);
          } else {
            resolve(this.myData.data.session.session_id);
          }
        });
    });
    return myPromise;
  }
  /*! recover lost password function, it takes email and token, and
  asks for a new password */
  recover(email) {
    const myPromise = new Promise((resolve, reject) => {
      const url = API_URL + 'user/resetpass';
      const jsonData = JSON.stringify({
        email,
        token: TOKEN.KEY
      });
      console.log('DEBUG api recover, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
      data => {
        console.log('DEBUG api recover, sending:', data);
        this.myData = data;
        if (this.myData.result === 'error') {
          console.log(this.myData);
          reject(this.myData.msg);
        } else {
          resolve(this.myData.result);
        }
      });
    });
    return myPromise;
  }
  /*!function for logging out api session */
  logout(sessionId) {
    const myPromise = new Promise((resolve, reject) => {
      const url = API_URL + 'user/logout';
      const jsonData = JSON.stringify({
              sessionId,
              token: TOKEN.KEY
      });
      console.log('DEBUG api logout, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
        data => {
          this.myData = data;
          console.log('DEBUG api logout, receiving:', data);
          if (this.myData.result === 'error') {
            reject(this.myData.msg);
          } else {
            resolve(this.myData.data);
          }
        });
      });
    return myPromise;
   }
  /*! registry function, it takes name, email, pass and token,
  and try to do a registry */
  register(name, email, password) {
    const myPromise = new Promise((resolve, reject) => {
      const url = API_URL + 'user/register';
      const jsonData = JSON.stringify({
        name,
        email,
        pass: password,
        token: TOKEN.KEY
      });
      console.log('DEBUG api register, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
        data => {
          this.myData = data;
          console.log('DEBUG api register, receiving:', data);
          if (this.myData.result === 'error') {
            reject(this.myData.msg);
          } else {
            resolve(this.myData.data);
          }
      });
    });
    return myPromise;
  }
  /*! take user data: */
  userData(sessionId) {
    const myPromise = new Promise((resolve, reject) => {
      const url = API_URL + 'user/data';
      const jsonData = JSON.stringify({
            session_id: sessionId,
            token: TOKEN.KEY
      });
      console.log('DEBUG api userData, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
        data => {
          this.myData = data;
          console.log('DEBUG api userData, receiving:', data);
          if (this.myData.result === 'error') {
            reject(this.myData.msg);
          } else {
            resolve(this.myData.data);
          }
        });
    });
    return myPromise;
  }
  /*! edit user data: */
  editUser(newName, newEmail, newPass, sessionId) {
    const myPromise = new Promise((resolve, reject) => {
    const url = API_URL + 'user/edit';
    const jsonData = JSON.stringify({
      email: newEmail,
      pass: newPass,
      name: newName,
      session_id: sessionId,
      token: TOKEN.KEY
    });
    console.log('DEBUG api editUser, sending:', jsonData);
    this.http.post(url, jsonData).subscribe(
      data => {
        this.myData = data;
        console.log('DEBUG api editUser, receiving:', data);
        if (this.myData.result === 'error') {
          reject(this.myData.msg);
        } else {
          resolve(this.myData.data);
        }
      });
    });
    return myPromise;
  }
  //////////////////////////////////////////
  /* TIPS */
  //////////////////////////////////////////
  /*! take user data: */
  tipsList(sessionId) {
    const myPromise = new Promise((resolve, reject) => {
    const url = API_URL + 'advice/list';
    const jsonData = JSON.stringify({
            session_id: sessionId,
            token: TOKEN.KEY
    });
    console.log('DEBUG api tipsList, sending:', jsonData);
    this.http.post(url, jsonData).subscribe(
      data => {
        this.myData = data;
        console.log('DEBUG api tipsList, receiving:', data);
        if (this.myData.result === 'error') {
          reject(this.myData.msg);
        } else {
          resolve(this.myData.data);
        }
      });
    });
    return myPromise;
  }
  //////////////////////////////////////////
  /* EVENTS */
  //////////////////////////////////////////
  /*! get rendez vous list: */
  eventsList(sessionId): Promise<IEventsList> {
    return new Promise((resolve, reject) => {
    const url = API_URL + 'event/list';
    const jsonData = JSON.stringify({
            session_id: sessionId,
            token: TOKEN.KEY
    });
    console.log('DEBUG api eventsList, sending:', jsonData);
    this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api eventsList, receiving:', myData);
        if ( myData.result === 'error') {
          reject(myData);
        } else {
          resolve(myData);
        }
      });
    });
  }
  /*! get rendez vous item: */
  eventGet(eventId, sessionId): Promise<IEvent> {
    return new Promise((resolve, reject) => {
      const url = API_URL + 'event/item';
      const jsonData = JSON.stringify({
              event_id: eventId,
              session_id: sessionId,
              token: TOKEN.KEY
      });
      console.log('DEBUG api eventGet, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
        myData => {
          console.log('DEBUG api eventGet, receiving:', myData);
          if ( myData.result === 'error') {
            reject(myData);
          } else {
            resolve(myData);
          }
        });
    });
  }
  /*! funciton for creating an event, only description and date needed */
  eventAdd(date, description, sessionId) {
    return new Promise((resolve, reject) => {
      const url = API_URL + 'event/add';
      // YYYY-MM-DD HH:ii:ss
      const jsonData = JSON.stringify({
              datetime: date,
              description,
              sequence_id: 0,
              session_id: sessionId,
              token: TOKEN.KEY
      });
      console.log('DEBUG api eventAdd, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api eventAdd, receiving:', myData);
        if ( myData.result === 'error') {
          reject(0);
        } else {
          resolve(myData.data.event_id as number);
        }
      });
    });
  }
  /*! funciton for deleting an event */
  eventDelete(eventId, sessionId) {
    return new Promise((resolve, reject) => {
      const url = API_URL + 'event/delete';
      const jsonData = JSON.stringify({
            event_id: eventId,
            session_id: sessionId,
            token: TOKEN.KEY
      });
      console.log('DEBUG api eventDelete, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api eventDelete, receiving:', myData);
        if ( myData.result === 'error') {
          reject(0);
        } else {
          resolve(myData.result);
        }
      });
    });
  }
  /*! edit event item: */
  eventEdit(sequenceObject: IEvent, sessionId) {
    return new Promise((resolve, reject) => {
    const url = API_URL + 'event/edit';
    const dateString = this.dateForMysql(new Date(sequenceObject.datetime)) +
                            ' ' + new Date(sequenceObject.datetime).toTimeString().split(' ')[0];
    let sequencesString = '';
    for (const sequence of sequenceObject.sequences) {
      if (sequencesString.length === 0) {
        sequencesString = sequencesString + sequence.id;
      } else {
        sequencesString = sequencesString + ',' + sequence.id;
      }
    }
    const jsonData = JSON.stringify({
              event_id: sequenceObject.id,
              datetime: dateString,
              description: sequenceObject.description,
              sequences: sequencesString,
              session_id: sessionId,
              token: TOKEN.KEY
    });
    console.log('DEBUG api eventEdit, sending:', jsonData);
    this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api eventEdit, receiving:', myData);
        if ( myData.result === 'error') {
          reject(0);
        } else {
          resolve(myData.result);
        }
      });
    });
  }
  //////////////////////////////////////////
  /* SEQUENCES */
  //////////////////////////////////////////
  /*! get sequences list: */
  sequencesList(sessionId): Promise<ISequence[]>  {
    return new Promise((resolve, reject) => {
      const url = API_URL + 'sequence/list';
      const jsonData = JSON.stringify({
            session_id: sessionId,
            token: TOKEN.KEY
      });
      console.log('DEBUG api sequencesList, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
        myData => {
          console.log('DEBUG api sequencesList, receiving:', myData);
          if ( myData.result === 'error') {
            reject(myData.data);
          } else {
            resolve(myData.data);
          }
      });
    });
  }
  /*! get sequence item: */
  sequenceGet(sequenceId, sessionId) {
    return new Promise((resolve, reject) => {
      const url = API_URL + 'sequence/item';
      const jsonData = JSON.stringify({
            sequence_id: sequenceId,
            session_id: sessionId,
            token: TOKEN.KEY
      });
      console.log('DEBUG api sequenceGet, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
        myData => {
          console.log('DEBUG api sequenceGet, receiving:', myData);
          if ( myData.result === 'error') {
            reject(myData);
          } else {
            resolve(myData);
          }
      });
    });
  }
  /*! function for deleting a sequence */
  sequenceDelete(seqId, sessionId) {
    return new Promise((resolve, reject) => {
      const url = API_URL + 'sequence/delete';
      const jsonData = JSON.stringify({
            sequence_id: seqId,
            session_id: sessionId,
            token: TOKEN.KEY
      });
      console.log('DEBUG api sequenceDelete, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api sequenceDelete, receiving:', myData);
        if ( myData.result === 'error') {
          reject(0);
        } else {
          resolve(myData.result);
        }
      });
    });
  }
  /*! edit sequence item: */
  sequenceEdit(sequenceObject: ISequence, sessionId) {
    return new Promise((resolve, reject) => {
    const url = API_URL + 'sequence/edit';
    let pictogramsString = '';
    for (const sequence of sequenceObject.pictograms) {
      if (pictogramsString.length === 0) {
        pictogramsString = pictogramsString + sequence.id;
      } else {
        pictogramsString = pictogramsString + ',' + sequence.id;
      }
    }
    const jsonData = JSON.stringify({
              sequence_id: sequenceObject.id,
              title: sequenceObject.title,
              description: sequenceObject.description,
              pictograms: pictogramsString,
              session_id: sessionId,
              token: TOKEN.KEY
    });
    console.log('DEBUG api sequenceEdit, sending:', jsonData);
    this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api sequenceEdit, receiving:', myData);
        if ( myData.result === 'error') {
          reject(0);
        } else {
          resolve(myData.result);
        }
      });
    });
  }
  /*! add sequence item: */
  sequenceAdd(sequenceObject: ISequence, sessionId) {
    return new Promise((resolve, reject) => {
      const url = API_URL + 'sequence/add';

      const jsonData = JSON.stringify({
                title: sequenceObject.title,
                description: sequenceObject.description,
                session_id: sessionId,
                token: TOKEN.KEY
      });
      console.log('DEBUG api sequenceAdd, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
        myData => {
          console.log('DEBUG api sequenceAdd, receiving:', myData);
          if ( myData.result === 'error') {
            reject(0);
          } else {
            resolve(myData.data);
          }
      });
    });
  }
 //////////////////////////////////////////
  /* PICTOGRAMS */
  //////////////////////////////////////////
  /*! get pictograms list: */
  pictogramList(sessionId): Promise<IPictogram[]> {
    return new Promise((resolve, reject) => {
      const url = API_URL + 'pictogram/list';
      const jsonData = JSON.stringify({
            session_id: sessionId,
            token: TOKEN.KEY
      });
      console.log('DEBUG api pictogramList, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
        myData => {
          console.log('DEBUG api pictogramList, receiving:', myData);
          if ( (myData as any).result === 'error') {
            reject(myData);
          } else {
            resolve((myData as any).data);
          }
      });
    });
  }
  /*! get pictogram item: */
  pictogramItem(pictogramId, sessionId) {
    return new Promise((resolve, reject) => {
      const url = API_URL + 'pictogram/item';
      const jsonData = JSON.stringify({
            pictogram_id: pictogramId,
            session_id: sessionId,
            token: TOKEN.KEY
      });
      console.log('DEBUG api pictogramItem, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
        myData => {
          console.log('DEBUG api pictogramItem, receiving:', myData);
          if ( myData.result === 'error') {
            reject(myData);
          } else {
            resolve(myData);
          }
      });
    });
  }
  ///////////////////////////////////////////
  /*! MISCELLANEOUS FUNCTIONS*/
  ///////////////////////////////////////////
  /*! datetime to MySQL datetime function */
  dateForMysql(date: Date): string {
    let retDate, year, month, day: string;
    year = String(date.getFullYear());
    month = String(date.getMonth() + 1);
    if (month.length === 1) {
        month = '0' + month;
    }
    day = String(date.getDate());
    if (day.length === 1) {
        day = '0' + day;
    }
    retDate = year + '-' + month + '-' + day;
    return retDate;
  }
}
