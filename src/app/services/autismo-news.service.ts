/* 
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *  
 *  This file is part of Dentaltea
 *  
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of AppAutismoCórdoba
 *
 *  AppAutismoCórdoba is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  AppAutismoCórdoba is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with AppAutismoCórdoba.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/delay';

const BASE_URL = 'http://www.autismocordoba.org/wp-json/wp/v2/';

type Post = {
    title: string,
    link: string,
    pictureUrl: string
};
/*
export interface Media {
  source_url: string;
}

export interface Post {
    id: number;
    title: {
      rendered: string
    };
    link: string;
    featured_media: number;
    pictureUrl: Media;
}
*/
@Injectable({
  providedIn: 'root'
})

export class AutismoNewsService {
  myPosts: Post[];
  postIndex: number[] = [0, 1, 2, 3, 4];
  constructor(public http: HttpClient) { }

  getPosts(limit: any) {
    const myPromise = new Promise((resolve, reject) => {
      this.myPosts = new Array(limit);
      const url = BASE_URL + 'posts';
      this.http.get(url).subscribe(
      (posts) => {
        this.postIndex.forEach((index) => {
          const pictureUrl = BASE_URL + 'media/' + posts[index].featured_media;
          this.http.get(pictureUrl).subscribe(
            (imageUrl: any) => {
              console.log(imageUrl);
              this.myPosts[index] = {
                title: posts[index].title.rendered,
                link: posts[index].link,
                pictureUrl: imageUrl.source_url
              };
            },
            (noPicture) => {
              this.myPosts[index] = {
                title: posts[index].title.rendered,
                link: posts[index].link,
                pictureUrl: '../../../assets/images/ac_logo.png'
              };
          });
        });
        resolve(this.myPosts);
       },
      (err) => {
        this.myPosts[0] = {
          title: 'error',
          link: 'error',
          pictureUrl: ''
        };
        reject(this.myPosts);
      });
    });
    return myPromise;
  }
  /*
  getPosts(limit: number): Observable <Post[]>  {
    // limit is 5
    let NewsArray: Post[];
    const url = BASE_URL + 'posts';
    return this.http.get(url).map(
      (res) => {
      const response: any = res;
      response.map(
        (item) => {
          NewsArray = item;
          this.postIndex.forEach((index) => {
            const pictureUrl = BASE_URL + 'media/' + NewsArray[index].featured_media;
            this.http.get(pictureUrl).subscribe(
              (itemImage: any) => {
                console.log(itemImage);
                const imageUrl: Media = itemImage;
                NewsArray[index].pictureUrl = imageUrl;
                return NewsArray;
              },
              (noPicture: any) => {
                console.log(noPicture);
                const imageUrl: Media = noPicture;
                imageUrl.source_url = '../../../assets/images/ac_logo.png';
                NewsArray[index].pictureUrl = imageUrl;
                return NewsArray;
            });
          });
          return NewsArray;
      });
      return NewsArray;
    });
  }*/
}
