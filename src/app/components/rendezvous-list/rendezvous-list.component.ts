/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { IEvent } from '../../services/pixapi.service';
import { ModalController } from '@ionic/angular';
import { CalendarDayPage } from '../../pages/calendar-day/calendar-day.page';

export interface IRdvzList {
  day: number;
  month: string;
  year: number;
  dayData: IEvent;
}
export interface IShowList {
  [key: string]:
  {
    day: number;
    month: string;
    year: number;
    dayData: IEvent [];
  };
}

@Component({
  selector: 'app-rendezvous-list',
  templateUrl: './rendezvous-list.component.html',
  styleUrls: ['./rendezvous-list.component.scss'],
})
export class RendezvousListComponent implements OnInit, OnChanges {
@Input() rdvzList: IRdvzList[];
myList: IShowList;
now: Date;
yearsToShow: number[];
monthsToShow: string[][];
dropdownIcon: string[][];
monthShowStep: number;
months = [
  'Enero',
  'Febrero',
  'Marzo',
  'Abril',
  'Mayo',
  'Junio',
  'Julio',
  'Agosto',
  'Septiembre',
  'Octubre',
  'Noviembre',
  'Diciembre'
];
weekdays = [
  'Domingo',
  'Lunes',
  'Martes',
  'Miércoles',
  'Jueves',
  'Viernes',
  'Sábado'
];

  constructor(private modalCtrl: ModalController) {
    this.yearsToShow = [];
    this.monthsToShow = [];
    this.dropdownIcon = [];
  }

  ngOnInit() {
  }
  ngOnChanges() {
    this.myList = {};
    let sDay: string;
    let i = 0;
    if (this.rdvzList) {
      for (const rendezvous of this.rdvzList) {        
        if (!isNaN(rendezvous.day) && !isNaN(rendezvous.year)) {
          sDay = '';
          if ( rendezvous.day < 10) {
            sDay = '0';
          }
          sDay = sDay + rendezvous.day.toString();
          if (this.myList[rendezvous.year.toString() + rendezvous.month + sDay]) {
            this.myList[rendezvous.year.toString() + rendezvous.month + sDay].dayData.push(rendezvous.dayData);
          } else {
            this.myList[rendezvous.year.toString() + rendezvous.month + sDay] = {
              day: rendezvous.day,
              month: rendezvous.month,
              year: rendezvous.year,
              dayData: []
            };
            this.myList[rendezvous.year.toString() + rendezvous.month + sDay].dayData.push(rendezvous.dayData);
          }
          if (!this.yearsToShow.includes(rendezvous.year)) {
            this.yearsToShow.push(rendezvous.year);
            this.monthsToShow[rendezvous.year] = [];
            this.dropdownIcon[rendezvous.year] = [];
          }
          if (!this.monthsToShow[rendezvous.year].includes(rendezvous.month)) {
            this.monthsToShow[rendezvous.year].push(rendezvous.month);
            this.dropdownIcon[rendezvous.year][rendezvous.month] = 'caret-up';
            if (rendezvous.year === new Date().getFullYear() && rendezvous.month === this.months[new Date().getMonth()]) {
              this.dropdownIcon[rendezvous.year][rendezvous.month] = 'caret-down';
            }
          }
          i++;
        }
      }
      /*! sorting list by day, for fastest future loops */
      let sortedList = {};
      Object.keys(this.myList).sort((a, b) => this.myList[a].day - this.myList[b].day).forEach((key) => {
          sortedList[key] = this.myList[key]; });
      this.myList = sortedList;
    }
  }
  /* dropdown a month */
  async onToggleMonth(month, year) {
    for (let i=0; i < this.yearsToShow.length; i++) {
      for (let j=0; j < this.monthsToShow[this.yearsToShow[i]].length; j++) {
        this.dropdownIcon[this.yearsToShow[i]][this.monthsToShow[this.yearsToShow[i]][j]] = 'caret-up';
      }
    }
    this.dropdownIcon[year][month] = 'caret-down';

    return;
  }
  /* open editing dialog for an event */
  async onSelect(event: IShowList, key: string) {
    const daysLoaded: IEvent[] = [];
    const dayDate = new Date();
    dayDate.setFullYear(event[key].year);
    dayDate.setMonth(this.months.indexOf(event[key].month));
    dayDate.setDate(event[key].day);

    const modal = await this.modalCtrl.create({
      component: CalendarDayPage,
      componentProps: {
        date: dayDate, // passing date without time
        dayData: event[key].dayData // passing JSON data if exists
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.dismissed === true) {
      console.log('updating!');
    }
    return;
  }
  /*! evaluate rendezvous by year and month */
  evalRendezVous(year: number, month: string, event: IShowList, key: string) {
    let ret = false;
    if (event[key].year === year && event[key].month === month) {
      ret = true;
    }
    return ret;
  }
  /*! function for evaluate if a year is current year */
  isCurrentYear(year: number) {
    let ret = false;
    if (year === new Date().getFullYear()) {
      ret = true;
    }
    return ret;
  }
  /*! function for evaluate if a year is old */
  isOldYear(year: number) {
    let ret = false;
    if (year < new Date().getFullYear()) {
      ret = true;
    }
    return ret;
  }
  /*! function for evaluate if a month is current month */
  isCurrentMonth(year: number, month: string) {
    let ret = false;
    if (this.isCurrentYear(year) && month === this.months[new Date().getMonth()]) {
      ret = true;
    }
    return ret;
  }
  /*! function for evaluate if a month is old */
  isOldMonth(year: number, month: string) {
    let ret = false;
    if (this.isOldYear(year)) {
      ret = true;
    }
    if ( this.isCurrentYear(year) && this.months.indexOf(month) < new Date().getMonth()) {
      ret = true;
    }
    return ret;
  }
  /*! function for evaluate if a day is today */
   isCurrentDay(year: number, month: string, day: number) {
    let ret = false;
    if (this.isCurrentYear(year) && this.isCurrentMonth(year, month) && day === new Date().getDate()) {
      ret = true;
    }
    return ret;
  }
  /*! function for evaluate a day is old */
  isOldDay(year: number, month: string, day: number) {
    let ret = false;
    if (this.isOldYear(year)) {
      ret = true;
    }
    if ( this.isCurrentYear(year) && this.isOldMonth(year, month)) {
      ret = true;
    }
    if ( this.isCurrentYear(year) && this.isCurrentMonth(year, month) && day < new Date().getDate()) {
      ret = true;
    }
    return ret;
  }
}
