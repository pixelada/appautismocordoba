/* 
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *  
 *  This file is part of Dentaltea
 *  
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input } from '@angular/core';
import { PixapiService } from '../../services/pixapi.service';

@Component({
  selector: 'app-tip',
  templateUrl: './tip.component.html',
  styleUrls: ['./tip.component.scss'],
})
export class TipComponent implements OnInit {
  toggled: boolean;
  IMAGES_URL: string;
  dropdownIcon: string;
  @Input() tip: any;

  constructor(public myApi: PixapiService) {
    this.IMAGES_URL = this.myApi.BASE_URL;
  }

  ngOnInit() {
    this.toggled = false;
    this.dropdownIcon = 'caret-down';
  }

  toggle() {
    if (this.toggled) {
      this.toggled = false;
      this.dropdownIcon = 'caret-down';
    } else {
      this.toggled = true;
      this.dropdownIcon = 'caret-up';
    }
  }
}
