/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { ISequence, PixapiService } from '../../services/pixapi.service';
import { ModalController } from '@ionic/angular';
import { ChooseSequencePage } from '../../pages/choose-sequence/choose-sequence.page';
import { SequenceEditionPage } from '../../pages/sequence-edition/sequence-edition.page';
import { RendezvousService } from '../../services/rendezvous.service';
import { SequencesService } from '../../services/sequences.service';

@Component({
  selector: 'app-rendezvous-day-sequence-section',
  templateUrl: './rendezvous-day-sequence-section.component.html',
  styleUrls: ['./rendezvous-day-sequence-section.component.scss'],
})
export class RendezvousDaySequenceSectionComponent implements OnInit, OnChanges {
  @Input() allSequences: ISequence[];
  @Input() rdvzSequences: ISequence[];
  @Input() step: any;
  @Output() updatedSeq = new EventEmitter<ISequence[]>();
  // @Output() selectedSeq = new EventEmitter<ISequence>();

  constructor( private modalCtrl: ModalController, public myApi: PixapiService,
               private seqsService: SequencesService) {
    console.log('in constructor of rendezvous day sequence section component, step: ' + this.step);
    if (!this.rdvzSequences) {
      console.log('DANGER! rendezvous sequences: Undefined');
    }
    if (!this.allSequences) {
      console.log('DANGER! all sequences: Undefined');
    }
    console.log('exiting constructor of rendezvous day sequence section component');
  }
  ngOnInit() {

  }
  ngOnChanges() {
    if (this.rdvzSequences) {
      console.log('in ngOnChanges, rendezvous sequences:' + this.rdvzSequences);
      for (let i = 0; i < this.rdvzSequences.length; i++) {
        this.seqsService.getOneSequence(this.rdvzSequences[i].id).then(
          (val) => {
            this.rdvzSequences[i] = val as ISequence;
            console.log('image URL:' + this.myApi.BASE_URL + this.rdvzSequences[i].pictograms[0].image);
          });
      }
    } else {
      console.log('in ngOnChanges, rendezvous sequences: UNdefined');
    }
  }
  /*! function for selecting a new sequence */
  async selectSequence() {
    const modal = await this.modalCtrl.create({
      component: ChooseSequencePage,
      componentProps: {
        mode: 'new',
        allSequences: this.allSequences,
        selectedSequence: undefined,
        editable: true
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.sequence !== undefined) {
      this.rdvzSequences.push(data.sequence);
      this.updatedSeq.emit(this.rdvzSequences);
    }
    return;
  }
   /*! function for previewing a sequence */
   async preview(index) {
    const modal = await this.modalCtrl.create({
      component: SequenceEditionPage,
      componentProps: {
        new: false, // new item
        seqData: this.rdvzSequences[index],
        editable: false
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
    return;
  }
  /*! function for editing a sequence */
  async edit(index) {
    const modal = await this.modalCtrl.create({
      component: ChooseSequencePage,
      componentProps: {
        mode: 'edit',
        allSequences: this.allSequences,
        selectedSequence: this.rdvzSequences[index]
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.sequence !== undefined) {
      this.rdvzSequences[index] = data.sequence;
      this.updatedSeq.emit(this.rdvzSequences);
    }
    return;
  }
  /*! function for removing a sequence */
  async remove(index) {
    if (index >= 0 && index < this.rdvzSequences.length) {
      this.rdvzSequences.splice(index, 1);
      this.updatedSeq.emit(this.rdvzSequences);
    }
    return;
  }
  /*! function for moving sequences order */
  async reorder(reorderTo, index) {
    if ((reorderTo === -1 && index > 0) ||
    (reorderTo === 1 && index < (this.rdvzSequences.length - 1))) {
      const sequenceToMove = this.rdvzSequences.splice(index, 1);
      this.rdvzSequences.splice(index + reorderTo, 0, sequenceToMove[0]);
      this.updatedSeq.emit(this.rdvzSequences);
    }
  }
}
