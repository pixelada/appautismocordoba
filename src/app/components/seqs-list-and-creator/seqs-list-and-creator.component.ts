/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { ISequence, PixapiService } from '../../services/pixapi.service';
import { SequencesService } from '../../services/sequences.service';
import { ModalController } from '@ionic/angular';
import { SequenceEditionPage } from '../../pages/sequence-edition/sequence-edition.page';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-seqs-list-and-creator',
  templateUrl: './seqs-list-and-creator.component.html',
  styleUrls: ['./seqs-list-and-creator.component.scss'],
})
export class SeqsListAndCreatorComponent implements OnInit, OnChanges {
  @Input() allSequences: ISequence[];
  @Output() newSeqRet = new EventEmitter<ISequence[]>();
  @Output() removeSeqRet = new EventEmitter<ISequence>();
  @Output() editSeqRet = new EventEmitter<ISequence>();
  @Output() viewSeqRet = new EventEmitter<ISequence>();
  userId: any;
  constructor(private seqsService: SequencesService, public myApi: PixapiService,
              private modalCtrl: ModalController, private myStorage: Storage) {
    this.myStorage.get(this.myApi.USER_ID).then(
      (val) => {
        this.userId = val as any;
    });
  }
  ngOnInit() {}
  ngOnChanges() {
    if (this.allSequences) {
      for (let i = 0; i < this.allSequences.length; i++) {
        this.seqsService.getOneSequence(this.allSequences[i].id).then(
          (val) => {
            this.allSequences[i] = val as ISequence;
          });
      }
    }
  }
  /*! function for selecting a new sequence */
  async newSeq() {
    const modal = await this.modalCtrl.create({
      component: SequenceEditionPage,
      componentProps: {
        new: true, // new item
        seqData: {
          id: 0,
          title: '',
          description: '',
          pictograms: []
        }
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.modified !== undefined) { // we created one sequence
      if(this.allSequences === undefined) {
        this.allSequences = [];
      }
      this.allSequences.push(data.modified);
    }
    this.newSeqRet.emit(this.allSequences);
    return;
  }
  /*! function for editing a sequence */
  async editSeq(index) {
    this.editSeqRet.emit(this.allSequences[index]);
    return;
  }
  /*! function for removing a sequence */
  async removeSeq(index) {
    console.log('debug: emitting removing event from component');
    this.removeSeqRet.emit(this.allSequences[index]);
    return;
  }
    /*! function for only viewing a sequence */
    async onlyViewSeq(index) {
      this.viewSeqRet.emit(this.allSequences[index]);
      return;
    }
}
