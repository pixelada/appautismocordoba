/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ISequence, PixapiService } from '../../services/pixapi.service';

@Component({
  selector: 'app-sequence-section',
  templateUrl: './sequence-section.component.html',
  styleUrls: ['./sequence-section.component.scss'],
})
export class SequenceSectionComponent implements OnInit {
@Input() allSequences: ISequence [];
@Output() selectedSeq = new EventEmitter<ISequence>();
mySequence: ISequence = {
  author_id: '',
  description: '',
  id: 0,
  pictograms: [],
  title: ''
};
  constructor( public myApi: PixapiService) {
  }

  ngOnInit() {}

  select(selectedSeq) {
    if (this.mySequence.id !== selectedSeq.id) {
      this.mySequence = selectedSeq;
      this.selectedSeq.emit(this.mySequence);
    } else {
      this.mySequence.id = 0;
      this.selectedSeq.emit(this.mySequence);
    }
  }
}
