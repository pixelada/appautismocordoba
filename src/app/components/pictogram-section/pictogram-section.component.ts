/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { IPictogram, PixapiService } from '../../services/pixapi.service';
import { ModalController } from '@ionic/angular';
import { ChoosePictoPage } from '../../pages/choose-picto/choose-picto.page';
import { PictogramsService } from '../../services/pictograms.service';

@Component({
  selector: 'app-pictogram-section',
  templateUrl: './pictogram-section.component.html',
  styleUrls: ['./pictogram-section.component.scss'],
})
export class PictogramSectionComponent implements OnInit, OnChanges {
  @Input() allPictos: IPictogram[];
  @Input() seqPictos: IPictogram[];
  @Input() step: any;
  @Output() updatedPicto = new EventEmitter<IPictogram[]>();
  constructor(public myApi: PixapiService, private modalCtrl: ModalController,
              public pictoSvc: PictogramsService) {
              }
  ngOnInit() {}
  ngOnChanges() {
    if (this.seqPictos) {
      for (let i = 0; i < this.seqPictos.length; i++) {
        this.pictoSvc.getOnePictogram(this.seqPictos[i].id).then(
          (val) => {
            this.seqPictos[i] = val as IPictogram;
          });
      }
    }
  }
  async newPicto() {
    const modal = await this.modalCtrl.create({
      component: ChoosePictoPage,
      componentProps: {
        mode: 'new',
        allPictograms: this.allPictos,
        selectedPictogram: undefined
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.pictogram !== undefined) {
      this.seqPictos.push(data.pictogram);
      this.updatedPicto.emit(this.seqPictos);
    }
    return;
  }

  /*! function for moving pictograms order */
  async reorder(reorderTo, index) {
    if ((reorderTo === -1 && index > 0) ||
    (reorderTo === 1 && index < (this.seqPictos.length - 1))) {
      const pictoToMove = this.seqPictos.splice(index, 1);
      this.seqPictos.splice(index + reorderTo, 0, pictoToMove[0]);
      this.updatedPicto.emit(this.seqPictos);
    }
  }

  async edit(index) {
    const modal = await this.modalCtrl.create({
      component: ChoosePictoPage,
      componentProps: {
        mode: 'edit',
        allPictograms: this.allPictos,
        selectedPictogram: this.seqPictos[index]
      },
      cssClass: 'sequences-modal-class'
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.sequence !== undefined) {
      this.seqPictos[index] = data.pictogram;
      this.updatedPicto.emit(this.seqPictos);
    }
    return;
  }

  /*! function for removing a pictogram */
  async remove(index) {
    if (index >= 0 && index < this.seqPictos.length) {
      this.seqPictos.splice(index, 1);
      this.updatedPicto.emit(this.seqPictos);
    }
    return;
  }
}
