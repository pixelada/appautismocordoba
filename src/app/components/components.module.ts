/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { TipComponent } from './tip/tip.component';
import { RendezvousDaySectionComponent } from './rendezvous-day-section/rendezvous-day-section.component';
import { FormsModule } from '@angular/forms';
import { RendezvousDaySequenceSectionComponent } from './rendezvous-day-sequence-section/rendezvous-day-sequence-section.component';
import { SequenceSectionComponent } from './sequence-section/sequence-section.component';
import { RendezvousListComponent } from './rendezvous-list/rendezvous-list.component';
import { SeqsListAndCreatorComponent } from './seqs-list-and-creator/seqs-list-and-creator.component';
import { PictogramSectionComponent } from './pictogram-section/pictogram-section.component';
import { ChoosePictoSectionsComponent } from './choose-picto-sections/choose-picto-sections.component';

@NgModule({
  declarations: [
    HeaderComponent,
    TipComponent,
    RendezvousDaySectionComponent,
    RendezvousDaySequenceSectionComponent,
    SequenceSectionComponent,
    RendezvousListComponent,
    SeqsListAndCreatorComponent,
    PictogramSectionComponent,
    ChoosePictoSectionsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  exports: [
    HeaderComponent,
    TipComponent,
    RendezvousDaySectionComponent,
    RendezvousDaySequenceSectionComponent,
    SequenceSectionComponent,
    RendezvousListComponent,
    SeqsListAndCreatorComponent,
    PictogramSectionComponent,
    ChoosePictoSectionsComponent
  ]
})
export class ComponentsModule { }
