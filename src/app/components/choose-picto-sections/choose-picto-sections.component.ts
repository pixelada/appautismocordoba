/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IPictogram, PixapiService } from '../../services/pixapi.service';

@Component({
  selector: 'app-choose-picto-sections',
  templateUrl: './choose-picto-sections.component.html',
  styleUrls: ['./choose-picto-sections.component.scss'],
})
export class ChoosePictoSectionsComponent implements OnInit {
  @Input() allPictos: IPictogram [];
  @Output() selectedPicto = new EventEmitter<IPictogram>();
  myPicto: IPictogram = {
    id: 0,
    image: '',
    description: ''
  };
  constructor(public myApi: PixapiService) {
    console.log(this.allPictos);
  }

  ngOnInit() {}

  select(selectedPicto) {
    if (this.myPicto.id !== selectedPicto.id) {
      this.myPicto = selectedPicto;
      this.selectedPicto.emit(this.myPicto);
    } else {
      this.myPicto.id = 0;
      this.selectedPicto.emit(this.myPicto);
    }
  }
}
