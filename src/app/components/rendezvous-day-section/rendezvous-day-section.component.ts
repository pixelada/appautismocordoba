/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input } from '@angular/core';
import { IEvent } from '../../services/pixapi.service';

@Component({
  selector: 'app-rendezvous-day-section',
  templateUrl: './rendezvous-day-section.component.html',
  styleUrls: ['./rendezvous-day-section.component.scss'],
})
export class RendezvousDaySectionComponent implements OnInit {
  @Input() dayData: IEvent;
  title: string;
  time: string;
  picUrl: string;
  mode: string;
  constructor() { }

  ngOnInit() {
    /*for (const key of Object.keys(this.dayData)) {
      const value = this.dayData[key];
      console.log(`${key} -> ${value}`);
    }*/
    this.title = this.dayData.description;
    this.time = new Date(this.dayData.datetime).toISOString();
    const nowDate: Date = new Date();
    if ((nowDate.getFullYear() > new Date(this.dayData.datetime).getFullYear()) ||
    (nowDate.getFullYear() === new Date(this.dayData.datetime).getFullYear() &&
    nowDate.getMonth() > new Date(this.dayData.datetime).getMonth()) ||
    (nowDate.getFullYear() === new Date(this.dayData.datetime).getFullYear() &&
    nowDate.getMonth() === new Date(this.dayData.datetime).getMonth() &&
    nowDate.getDate() > new Date(this.dayData.datetime).getDate())) {
      this.mode = 'view';
    } else {
      this.mode = 'edition';
    }
  }

}
