/* 
 *  This is the default license template.
 *  
 *  File: app.po.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

import { browser, by, element } from 'protractor';
export class AppPage {
    navigateTo() {
        return browser.get('/');
    }
    getParagraphText() {
        return element(by.deepCss('app-root ion-content')).getText();
    }
}
//# sourceMappingURL=app.po.js.map