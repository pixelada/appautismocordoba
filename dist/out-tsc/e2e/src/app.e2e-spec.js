/* 
 *  This is the default license template.
 *  
 *  File: app.e2e-spec.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

import { AppPage } from './app.po';
describe('new App', () => {
    let page;
    beforeEach(() => {
        page = new AppPage();
    });
    it('should be blank', () => {
        page.navigateTo();
        expect(page.getParagraphText()).toContain('Start with Ionic UI Components');
    });
});
//# sourceMappingURL=app.e2e-spec.js.map