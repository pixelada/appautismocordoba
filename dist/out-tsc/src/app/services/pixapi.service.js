/* 
 *  This is the default license template.
 *  
 *  File: pixapi.service.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
const TOKEN_KEY = 'auth-token';
const BASE_URL = 'https://dev.pixelada.org/autismo/api/';
let PixapiService = class PixapiService {
    constructor() { }
    login(email, password) {
        return;
    }
    logout(sessionId) {
        return;
    }
};
PixapiService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__metadata("design:paramtypes", [])
], PixapiService);
export { PixapiService };
//# sourceMappingURL=pixapi.service.js.map