/* 
 *  This is the default license template.
 *  
 *  File: session.service.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { PixapiService } from './pixapi.service';
let SessionService = 
/* class for managing session, simply login, logout and status */
class SessionService {
    constructor(myStorage, myApi) {
        this.myStorage = myStorage;
        this.myApi = myApi;
    }
    login(email, pass) {
        this.myApi.login(email, pass);
        /*.then()
        .catch();*/
    }
};
SessionService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
    /* class for managing session, simply login, logout and status */
    ,
    tslib_1.__metadata("design:paramtypes", [Storage, PixapiService])
], SessionService);
export { SessionService };
//# sourceMappingURL=session.service.js.map