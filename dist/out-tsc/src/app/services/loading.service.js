/* 
 *  This is the default license template.
 *  
 *  File: loading.service.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
let LoadingService = 
/*! class for generic loading widget */
class LoadingService {
    constructor(loadingController) {
        this.loadingController = loadingController;
        this.currentLoading = null;
    }
    // show loading widget
    present(message = null, duration = null) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            // Dismiss previously created loading
            if (this.currentLoading != null) {
                this.currentLoading.dismiss();
            }
            this.currentLoading = yield this.loadingController.create({
                duration,
                message
            });
            return yield this.currentLoading.present();
        });
    }
    // hide loading widget
    dismiss() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.currentLoading != null) {
                yield this.loadingController.dismiss();
                this.currentLoading = null;
            }
            return;
        });
    }
};
LoadingService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
    /*! class for generic loading widget */
    ,
    tslib_1.__metadata("design:paramtypes", [LoadingController])
], LoadingService);
export { LoadingService };
//# sourceMappingURL=loading.service.js.map