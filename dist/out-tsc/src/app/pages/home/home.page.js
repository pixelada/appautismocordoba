/* 
 *  This is the default license template.
 *  
 *  File: home.page.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

/*
 *  Copyright 2020 Pixelada S. C. A.
 *
 *  This file is part of appAutismoCordoba.
 *
 *  appAutismoCordoba is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  appAutismoCordoba is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with appAutismoCordoba.  If not, see <https://www.gnu.org/licenses/>.
 *  This is a license template.
 */
import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { LoadingService } from '../../services/loading.service';
import { Router } from '@angular/router';
let HomePage = class HomePage {
    constructor(loading, router) {
        this.loading = loading;
        this.router = router;
    }
    ngOnInit() {
        console.log('DEBUG: ngOnInit');
        // showing loading widget
        this.loading.present('Espere');
        this.registered = false;
    }
    ionViewDidEnter() {
        console.log('DEBUG: ionViewDidLoad');
        // TODO: check if we have an already logged session
        setTimeout(() => {
            this.registered = this.amIRegistered();
        }, 5000);
        // navigate to app or to login pages
        if (this.registered) {
            console.log('DEBUG: registered, going to main');
            this.router.navigate(['main']);
        }
        else {
            console.log('DEBUG: unregistered, going to login');
            this.router.navigate(['login']);
        }
    }
    ionViewDidLeave() {
        console.log('DEBUG: leaving home page');
        // hide loading widget
        this.loading.dismiss();
    }
    amIRegistered() {
        return false;
    }
};
HomePage = tslib_1.__decorate([
    Component({
        selector: 'app-home',
        templateUrl: 'home.page.html',
        styleUrls: ['home.page.scss'],
    }),
    tslib_1.__metadata("design:paramtypes", [LoadingService, Router])
], HomePage);
export { HomePage };
//# sourceMappingURL=home.page.js.map