/* 
 *  This is the default license template.
 *  
 *  File: main.page.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let MainPage = class MainPage {
    constructor() { }
    ngOnInit() {
    }
};
MainPage = tslib_1.__decorate([
    Component({
        selector: 'app-main',
        templateUrl: './main.page.html',
        styleUrls: ['./main.page.scss'],
    }),
    tslib_1.__metadata("design:paramtypes", [])
], MainPage);
export { MainPage };
//# sourceMappingURL=main.page.js.map