/* 
 *  This is the default license template.
 *  
 *  File: main.module.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MainPageRoutingModule } from './main-routing.module';
import { MainPage } from './main.page';
let MainPageModule = class MainPageModule {
};
MainPageModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            IonicModule,
            MainPageRoutingModule
        ],
        declarations: [MainPage]
    })
], MainPageModule);
export { MainPageModule };
//# sourceMappingURL=main.module.js.map