/* 
 *  This is the default license template.
 *  
 *  File: main-routing.module.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MainPage } from './main.page';
const routes = [
    {
        path: '',
        component: MainPage
    }
];
let MainPageRoutingModule = class MainPageRoutingModule {
};
MainPageRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule],
    })
], MainPageRoutingModule);
export { MainPageRoutingModule };
//# sourceMappingURL=main-routing.module.js.map