/* 
 *  This is the default license template.
 *  
 *  File: login.page.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
let LoginPage = class LoginPage {
    constructor() { }
};
LoginPage = tslib_1.__decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.page.html',
        styleUrls: ['./login.page.scss'],
    }),
    tslib_1.__metadata("design:paramtypes", [])
], LoginPage);
export { LoginPage };
ngOnInit();
{
}
// function for login
login();
{
    console.log('DEBUG: login with email ', this.data.email, ' and password ', this.data.pass);
}
// function for register
register();
{
    console.log('going to registration');
}
//# sourceMappingURL=login.page.js.map