/* 
 *  This is the default license template.
 *  
 *  File: environment.prod.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

export const environment = {
    production: true
};
//# sourceMappingURL=environment.prod.js.map