/* 
 *  This is the default license template.
 *  
 *  File: zone-flags.js
 *  Author: joa
 *  Copyright (c) 2020 joa
 *  
 *  To edit this license information: Press Ctrl+Shift+P and press 'Create new License Template...'.
 */

/**
 * Prevents Angular change detection from
 * running with certain Web Component callbacks
 */
window.__Zone_disable_customElements = true;
//# sourceMappingURL=zone-flags.js.map